import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'example-three',
  templateUrl: 'three.component.html',
  styleUrls: ['three.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ExampleThreeComponent {}
