import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'stock-branch',
  templateUrl: 'stock-branch.component.html',
})
export class StockBranchComponent {
  @Input()
  parent: FormGroup;
}
