import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'stock-products',
  templateUrl: 'stock-products.component.html',
})
export class StockProductsComponent {
  @Input()
  parent: FormGroup;

  @Output()
  removed = new EventEmitter<any>();

  get stocks() {
    return (this.parent.get('stock') as FormArray).controls;
  }

  onRemove(group: any, index: any) {
    this.removed.emit({ group, index });
  }
}
