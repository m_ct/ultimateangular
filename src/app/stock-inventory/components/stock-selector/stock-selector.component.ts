import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormArray } from '@angular/forms';
import { Product } from '../../models/products.interface';

@Component({
  selector: 'stock-selector',
  templateUrl: 'stock-selector.component.html',
})
export class StockSelectorComponent {
  @Input()
  parent: FormGroup;

  @Input()
  products: Product[];

  @Output()
  added = new EventEmitter<any>();

  onAdd() {
    this.added.emit(this.parent.get('selector')?.value);
  }
}
