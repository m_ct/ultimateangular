import { Component } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Product } from '../../models/products.interface';

@Component({
  selector: 'stock-inventory',
  templateUrl: 'stock-inventory.component.html',
  styleUrls: ['stock-inventory.component.css'],
})
export class StockInventoryComponent {
  products: Product[] = [
    { id: 1, price: 100, name: '1-aaa' },
    { id: 2, price: 200, name: '2-aaa' },
    { id: 3, price: 300, name: '3-aaa' },
    { id: 4, price: 400, name: '4-aaa' },
    { id: 5, price: 500, name: '5-aaa' },
  ];

  // form = new FormGroup({
  //   store: new FormGroup({
  //     branch: new FormControl('abc'),
  //     code: new FormControl('1234'),
  //   }),
  //   //selector: new FormGroup({
  //   //  product_id: new FormControl(''),
  //   //  quantity: new FormControl(10),
  //   //}),
  //   selector: this.createStock({}),
  //   stock: new FormArray([
  //     this.createStock({ product_id: 7, quantity: 10 }),
  //     this.createStock({ product_id: 8, quantity: 80 }),
  //   ]),
  // });

  form = this.fb.group({
    store: this.fb.group({
      branch: '',
      code: '',
    }),
    selector: this.createStock({}),
    stock: this.fb.array([
      this.createStock({ product_id: 7, quantity: 10 }),
      this.createStock({ product_id: 8, quantity: 80 }),
    ]),
  });

  constructor(private fb: FormBuilder) {}

  createStock(stock: any) {
    return this.fb.group({
      product_id: parseInt(stock.product_id, 10) || '',
      quantity: stock.quantity || 10,
    });
  }

  onSubmit() {
    console.log('Submit:', this.form.value);
  }

  addStock(event: any) {
    const control = this.form.get('stock') as FormArray;
    control.push(this.createStock(event));
  }

  removeStock({ group, index }: { group: FormGroup; index: number }) {
    console.log(group, index);
    const control = this.form.get('stock') as FormArray;
    control.removeAt(index);
  }
}
