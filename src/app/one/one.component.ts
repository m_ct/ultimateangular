import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'example-one',
  templateUrl: 'one.component.html',
  styleUrls: ['one.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleOneComponent {
  @Input()
  user: any;

  update() {
    this.user.name = 'one';
  }
}
