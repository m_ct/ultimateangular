import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  TemplateRef,
  ChangeDetectionStrategy,
  OnInit,
} from '@angular/core';

import { User } from '../app/auth-form/auth-form.interface';

import { AuthForm2Component } from '../app/auth-form/auth-form2.component';
import { FileSizePipe } from '../app/filesize.pipe';

interface Nav {
  link: string;
  name: string;
  exact: boolean;
}

interface File {
  name: string;
  size: any;
  type: string;
}

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [FileSizePipe],
})
export class AppComponent implements OnInit {
  files: File[];
  mapped: File[];

  constructor(private fileSizePipe: FileSizePipe) {}

  ngOnInit(): void {
    this.files = [
      { name: 'aaa', size: 2120109, type: 'type-a' },
      { name: 'bbb', size: 18029, type: 'type-b' },
      { name: 'ccc', size: 1784562, type: 'type-c' },
    ];

    this.mapped = this.files.map((file) => {
      return {
        name: file.name,
        type: file.type,
        size: this.fileSizePipe.transform(file.size, 'mb'),
      };
    });
  }
}

/*
export class AppComponent {
  items = [
    { name: 'aaa', location: 'location-a' },
    { name: 'bbb', location: 'location-b' },
    { name: 'ccc', location: 'location-c' },
  ];

  constructor() {
    setTimeout(() => {
      //this.items.push({ name: 'ddd', location: 'location-d' });
      this.items = [...this.items, { name: 'ddd', location: 'location-d' }];
    }, 2000);
  }
}
*/
/*
export class AppComponent {
  user: any = {
    name: 'Motto',
    age: 44,
    location: 'UK',
  };

  addProp() {
    this.user.email = 'a@a.com';
  }

  changeUser() {
    this.user = {
      name: 'changed user',
      age: 41,
      location: 'California',
    };
  }

  changeName() {
    this.user.name = 'Another';
  }
}
*/
/*
export class AppComponent {
  
export class AppComponent implements AfterContentInit {
  @ViewChild('entry', { static: true, read: ViewContainerRef })
  entry: ViewContainerRef;
  component: ComponentRef<AuthForm2Component>;
  originalComponent: ComponentRef<AuthForm2Component>;

  @ViewChild('tmpl', { static: true }) tmpl: TemplateRef<any>;

  ctx = { $implicit: 'Todd M', location: 'England, UK' };

  constructor(private resolver: ComponentFactoryResolver) {
    console.log('dhgfduyavdfvkavuyagvyegvuaye');
  }

  ngAfterContentInit(): void {
    // const authFormFactory =
    //   this.resolver.resolveComponentFactory(AuthForm2Component);
    // this.originalComponent = this.entry.createComponent(authFormFactory);
    // this.component = this.entry.createComponent(authFormFactory, 0);
    // console.log(this.component.instance);
    // this.component.instance.title = 'Create account';
    // this.component.instance.submitted.subscribe(this.loginUser);
    // this.entry.createEmbeddedView(this.tmpl, {
    //   $implicit: 'Motto',
    //   location: 'England UK',
    // });
  }

  destroyComponent() {
    this.component.destroy();
  }

  moveComponent() {
    this.entry.move(this.component.hostView, 1);
  }

  rememberMe = false;
  createUser(user: any) {
    //console.log('create user', user);
  }

  loginUser(user: any) {
    console.log('login user', user);
  }

  rememberUser(evt: boolean) {
    this.rememberMe = evt;
    //console.log('remember? ', evt);
  }
  
  nav: Nav[] = [
    {
      link: '/',
      name: 'Home',
      exact: true,
    },
    {
      link: '/passengers',
      name: 'Passengers',
      exact: true,
    },
    {
      link: '/oops',
      name: '404',
      exact: false,
    },
  ];
}
*/
