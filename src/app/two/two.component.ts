import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'example-two',
  templateUrl: 'two.component.html',
  styleUrls: ['two.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.Default,
})
export class ExampleTwoComponent {
  @Input()
  user: any;

  update() {
    this.user.name = 'two';
  }
}
