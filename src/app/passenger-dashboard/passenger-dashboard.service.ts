import { Injectable } from '@angular/core';
import { Passenger } from './models/passenger.interface';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, tap, retry, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const PASSENGER_API: string = '/api/passengers';

@Injectable()
export class PassengerDashboardService {
  constructor(private http: HttpClient) {}

  getPassengers(): Observable<Passenger[]> {
    return this.http.get<Passenger[]>(PASSENGER_API).pipe(
      tap((x) => console.log('Before map', x)),
      map((res: Passenger[]) => res),
      catchError((error) => {
        console.log('Caught in CatchError. Throwing error');
        return of(error);
        //return throwError(error);
      })
    );
  }

  getPassenger(id: number): Observable<Passenger> {
    return this.http.get<Passenger>(`${PASSENGER_API}/${id}`).pipe(
      map((res: Passenger) => res),
      catchError((error) => {
        console.log('Caught in CatchError. Throwing error');
        return of(error);
        //return throwError(error);
      })
    );
  }

  updatePassenger(passenger: Passenger): Observable<Passenger> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    let options = { headers };

    const url = `${PASSENGER_API}/${passenger.id}`;

    return this.http.put<Passenger>(url, passenger, options).pipe(
      tap((x) => console.log('Before PUT map', x)),
      map((res: Passenger) => res),
      tap((x) => console.log('After PUT map', x)),
      catchError((error) => {
        console.log('Caught in CatchError. Throwing error');
        return of(error);
      })
    );
  }

  deletePassenger(passenger: Passenger): Observable<Passenger> {
    return this.http.delete<Passenger>(`${PASSENGER_API}/${passenger.id}`).pipe(
      tap((x) => console.log('Before PUT map', x)),
      map((res: Passenger) => res),
      tap((x) => console.log('After PUT map', x)),
      catchError((error) => {
        console.log('Caught in CatchError. Throwing error');
        return of(error);
      })
    );
  }
}
