import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';
import { Baggage } from '../../models/baggage.interface';

@Component({
  selector: 'passenger-form',
  template: `<form
    (ngSubmit)="handleSubmit(form.value, form.valid)"
    #form="ngForm"
    novalidate
  >
    <div>
      Passenger name:
      <input
        type="text"
        name="fullName"
        required
        #fullName="ngModel"
        [ngModel]="detail?.fullName"
      />
      {{ fullName.errors | json }}
      <div
        *ngIf="fullName.errors?.['required'] && fullName.dirty"
        class="error"
      >
        Passenger name is required
      </div>
    </div>
    <div>
      Passenger id:
      <input
        type="number"
        name="id"
        required
        #id="ngModel"
        [ngModel]="detail?.id"
      />
      {{ id.errors | json }}
      <div *ngIf="id.errors?.['required'] && id.dirty" class="error">
        ID is required
      </div>
    </div>
    <!-- div>
      <label>
        <input
          type="radio"
          [value]="true"
          name="checkedIn"
          [ngModel]="detail?.checkedIn"
          (ngModelChange)="toggleCheckIn($event)"
        />Yes
      </label>
      <label>
        <input
          type="radio"
          [value]="false"
          name="checkedIn"
          [ngModel]="detail?.checkedIn"
          (ngModelChange)="toggleCheckIn($event)"
        />No
      </label>
    </div -->

    <div>
      <label>
        <input
          type="checkbox"
          name="checkedIn"
          [ngModel]="detail?.checkedIn"
          (ngModelChange)="toggleCheckIn($event)"
        />
      </label>
    </div>

    <div *ngIf="form.value.checkedIn">
      CheckIn date:
      <input type="number" name="checkInDate" [ngModel]="detail?.checkInDate" />
    </div>

    <div>
      Luggage:
      <select name="baggage" [ngModel]="detail?.baggage">
        <option
          *ngFor="let item of baggage"
          [value]="item.key"
          [selected]="item.key === detail?.baggage"
        >
          {{ item.value }}
        </option>
      </select>
      <!-- select name="baggage" [ngModel]="detail?.baggage">
        <option *ngFor="let item of baggage" [ngValue]="item.key">
          {{ item.value }}
        </option>
      </select -->
    </div>

    <button type="submit" [disabled]="form.invalid">Update passenger</button>
  </form>`,
  styleUrls: ['passenger-form.component.scss'],
})
export class PassengerFormComponent {
  @Input()
  detail: Passenger;

  @Output()
  update: EventEmitter<Passenger> = new EventEmitter<Passenger>();

  baggage: Baggage[] = [
    {
      key: 'none',
      value: 'No baggage',
    },
    {
      key: 'hand-only',
      value: 'Hand baggage',
    },
    {
      key: 'hold-only',
      value: 'Hold baggage',
    },
    {
      key: 'hand-hold',
      value: 'Hand and hold baggage',
    },
  ];

  constructor() {}

  toggleCheckIn(checkedIn: boolean) {
    if (checkedIn === true) {
      this.detail.checkInDate = Date.now();
    }
  }

  handleSubmit(passenger: Passenger, isValid: boolean | null) {
    if (isValid) {
      this.update.emit(passenger);
    }
  }
}
