import { Component, OnInit } from '@angular/core';

import { PassengerDashboardService } from '../../passenger-dashboard.service';

import { Passenger } from '../../models/passenger.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'passenger-dashboard',
  styleUrls: ['passenger-dashboard.component.scss'],
  template: `
    <div>
      <passenger-count [items]="passengers"></passenger-count>
      <div *ngFor="let a of passengers">{{ a.fullName }}</div>
      <passenger-detail
        *ngFor="let varPassenger of passengers"
        [detail]="varPassenger"
        (remove)="handleRemove($event)"
        (edit)="handleEdit($event)"
        (view)="handleView($event)"
      ></passenger-detail>
    </div>
  `,
})
export class PassengerDashboardComponent implements OnInit {
  passengers: Passenger[];

  constructor(
    private router: Router,
    private passengerService: PassengerDashboardService
  ) {}

  ngOnInit(): void {
    //this.passengers = this.passengerService.getPassengers();

    // Create observer object
    const passengersObserver = {
      next: (data: Passenger[]) => {
        console.log('Observer data: ' + data);
        this.passengers = data;
      },
      error: (err: Error) => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };

    this.passengerService.getPassengers().subscribe(passengersObserver);

    /*
    this.passengerService.getPassengers().subscribe({
      next: (data: Passenger[]) => {
        console.log('data: ' + data);
        this.passengers = data;
      },
    });
    */
  }

  handleRemove(event: Passenger) {
    /*
    this.passengers = this.passengers.filter((passenger: Passenger) => {
      return passenger.id !== event.id;
    });
    */

    this.passengerService
      .deletePassenger(event)
      .subscribe((data: Passenger) => {
        this.passengers = this.passengers.filter((passenger: Passenger) => {
          return passenger.id !== event.id;
        });
      });
  }

  handleEdit(event: Passenger) {
    /*
    this.passengers = this.passengers.map((passenger: Passenger) => {
      if (passenger.id === event.id) {
        passenger = Object.assign({}, passenger, event);
      }
      return passenger;
    });
*/
    const pasObserver = {
      next: (data: Passenger) => {
        console.log('Observer data: ' + data);
        this.passengers = this.passengers.map((passenger: Passenger) => {
          if (passenger.id === event.id) {
            passenger = Object.assign({}, passenger, event);
          }
          return passenger;
        });
      },
      error: (err: Error) => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };
    this.passengerService.updatePassenger(event).subscribe(pasObserver);

    console.log(this.passengers);
  }

  handleView(event: Passenger) {
    this.router.navigate(['/passengers', event.id]);
  }
}
