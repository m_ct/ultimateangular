import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'auth-remember',
  template: `<div>
    <label>
      <input type="checkbox" (change)="onChecked($event)" />
      Keep me logged in
    </label>
  </div> `,
  styleUrls: ['auth-form.component.css'],
})
export class AuthRememberComponent {
  @Output()
  checkedEvt: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() {}

  onChecked(event: any) {
    //console.log('checked', event);
    this.checkedEvt.emit(event.target.checked);
  }
}
