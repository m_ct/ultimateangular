import {
  Component,
  Input,
  Output,
  EventEmitter,
  ContentChild,
  AfterContentInit,
  ContentChildren,
  QueryList,
  ViewChild,
  ViewChildren,
  AfterViewInit,
  OnInit,
  ChangeDetectorRef,
  ElementRef,
  Renderer2,
} from '@angular/core';
import { User } from './auth-form.interface';
import { AuthRememberComponent } from './auth-remember.component';
import { AuthMessageComponent } from './auth-message.component';

@Component({
  selector: 'auth-form',
  templateUrl: 'auth-form.component.html',
  /*
  template: `<div>
    <form (ngSubmit)="onSubmit(form.value)" #form="ngForm">
      <ng-content select="h3"></ng-content>
      <label>
        Email
        <input type="email" name="email" ngModel />
      </label>
      <label>
        Password
        <input type="password" name="password" ngModel />
      </label>
      <ng-content select="auth-remember"></ng-content>
      <!-- div *ngIf="showMessage">You will be logged for 30 days</div -->
      <!-- auth-message
        [style.display]="showMessage ? 'inherit' : 'none'"
      ></auth-message -->
      <auth-message [hidden]="showMessage"></auth-message>
      <ng-content select="button"></ng-content>
    </form>
  </div>`,
  */
  styleUrls: ['auth-form.component.css'],
})
export class AuthFormComponent
  implements AfterContentInit, AfterViewInit, OnInit
{
  user: User = <User>{};

  showMessage = false;

  @ContentChild(AuthRememberComponent)
  remember: AuthRememberComponent;

  //@ContentChildren(AuthRememberComponent)
  //remember: QueryList<AuthRememberComponent>;

  @ViewChild(AuthMessageComponent, { static: true })
  message: AuthMessageComponent;

  @ViewChildren(AuthMessageComponent)
  messages: QueryList<AuthMessageComponent>;

  @ViewChild('email')
  email: ElementRef;

  selectedMessage: AuthMessageComponent;

  @Output()
  submitted: EventEmitter<User> = new EventEmitter<User>();

  constructor(private renderer: Renderer2, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    console.log('on init', this.message);
  }

  ngAfterViewInit(): void {
    /*
    this.messages.changes.subscribe((msgs: QueryList<AuthMessageComponent>) => {
      console.log('changes');
      this.selectedMessage = msgs.first;
      this.selectedMessage.days = 17;
    });
*/
    if (this.messages) {
      this.messages.forEach((msg) => {
        msg.days = 30;
      });
      this.cd.detectChanges();
    }

    this.renderer.setAttribute(
      this.email.nativeElement,
      'placeholder',
      'Enter email address'
    );
    this.renderer.addClass(this.email.nativeElement, 'email');
    this.renderer.selectRootElement('#email').focus();

    //this.email.nativeElement.setAttribute(
    //  'placeholder',
    //  'Enter your email address'
    //);
    //this.email.nativeElement.classList.add('email');
    //this.email.nativeElement.focus();
  }

  ngAfterContentInit() {
    if (this.remember) {
      this.remember.checkedEvt.subscribe((checked: boolean) => {
        this.showMessage = checked;
      });
    }

    if (this.message) {
      this.message.days = 25;
    }

    /*
    if (this.remember) {
      this.remember.forEach((item) => {
        item.checkedEvt.subscribe((checked: boolean) => {
          this.showMessage = checked;
        });
      });
    }
    */
  }

  onSubmit(value: User) {
    this.submitted.emit(value);
  }
  test() {
    this.renderer.selectRootElement('#email').focus();
  }
}
