import {
  Directive,
  HostListener,
  HostBinding,
  OnInit,
  ViewContainerRef,
  Input,
} from '@angular/core';
import { ElementRef } from '@angular/core';

@Directive({
  selector: '[tooltip]',
  exportAs: 'tooltip',
})
export class TooltipDirective implements OnInit {
  tooltipElement = document.createElement('div');
  visible = false;

  @Input()
  set tooltip(value: string | null) {
    this.tooltipElement.textContent = value;
  }

  constructor(private element: ElementRef) {
    //console.log(this.element);
  }

  hide() {
    this.tooltipElement.classList.remove('tooltip--active');
    //console.log(this.tooltipElement.classList);
    this.tooltipElement.setAttribute('hidden', '');
  }

  show() {
    this.tooltipElement.classList.add('tooltip--active');
    //console.log(this.tooltipElement.classList);
    this.tooltipElement.removeAttribute('hidden');
  }

  ngOnInit(): void {
    this.tooltipElement.className = 'tooltip';
    this.element.nativeElement.appendChild(this.tooltipElement);
    this.element.nativeElement.classList.add('tooltip-container');
    //console.log(this.element.nativeElement.classList);
    this.tooltipElement.setAttribute('hidden', '');
    //console.log(this.tooltipElement.classList);
  }
}
