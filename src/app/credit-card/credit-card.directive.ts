import { Directive, HostListener, HostBinding } from '@angular/core';
import { ElementRef } from '@angular/core';

@Directive({
  selector: '[credit-card]',
})
export class CreditCardDirective {
  @HostBinding('style.border')
  border: string;

  @HostListener('input', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    //console.log(event);
    const input = event.target as HTMLInputElement;
    console.log(input.value);
    let trimmed = input.value.replace(/\s+/g, '');
    if (trimmed.length > 16) {
      trimmed = trimmed.substring(0, 16);
    }
    //console.log(trimmed);
    let numbers = [];
    for (let i = 0; i < trimmed.length; i += 4) {
      numbers.push(trimmed.substring(i, i + 4));
      //console.log(i, trimmed.substring(i, i + 4));
    }
    console.log(numbers);
    input.value = numbers.join(' ');

    this.border = '';
    if (/[^\d]+/.test(trimmed)) {
      this.border = '1px solid red';
    }
  }
  constructor(private element: ElementRef) {
    //console.log(this.element);
  }
}
